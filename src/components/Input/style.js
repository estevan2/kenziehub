import styled from "styled-components";

export const Container = styled.input`
    background-color: #fff;
    color: #000;
    font-size: 18px;
    border: 1px solid ${props => props.error ? "var(--red)" : "var(--blue)}"};
    border-radius: 10px;
    height: 56px;
    width: 100%;
    padding-left: 25px;

    &::placeholder {
            color: grey;
        }

    :focus {
        outline: none;
        border: 1px solid var(--blue-bold);
    }
`

export const Msg = styled.p`
    height: 10px;
    color: var(--red);
`