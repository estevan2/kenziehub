import { Container, Msg } from "./style"

const Input = ({ name, register, error, ...rest }) => {

    return (
        <>
            <Container error={!!error} {...rest} />
            <Msg>{error}</Msg>
        </>
    )
}

export default Input