import { Container } from "./style"
import { FiTrash2 } from "react-icons/fi"
import api from "../../services/api"
import { toast } from "react-toastify"
const Card = ({ id, title, status, created, updated, token, deleteTech }) => {

    return (
        <Container>
            <div className="trash_container">
                <button onClick={() => deleteTech(id)}><FiTrash2 /></button>
            </div>
            <div className="content">
                <h2>{title}</h2>
                <p>{status}</p>
                <p>Inicio: {created}</p>
                <p>Ultima atualização: {updated}</p>
            </div>
        </Container>
    )
}

export default Card