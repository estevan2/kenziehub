import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    background-color: #fff;
    border: 1px solid var(--blue);
    border-radius: 10px;
    width: 240px;
    height: 295px;
    padding: 10px;
    margin: 10px;

    .trash_container {
        display: flex;
        justify-content: flex-end;

        button {
            border: none;
            border-radius: 5px;
            background-color: var(--red);
            width: 36px;
            height: 36px;

            :hover {
                    filter: brightness(1.1);
            }

            svg {
                width: 26px;
                height: 26px;
            }
        }
    }

    .content {
        display: flex;
        flex-direction: column;
        justify-content: space-evenly;
        height: 70%;
        text-align: center;
    }
`