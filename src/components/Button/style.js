import styled from "styled-components";

export const Container = styled.button`
    background-color: var(--blue-bold);
    font-size: 24px;
    color: #fff;
    width: 164px;
    height: 48px;
    border-radius: 10px;
    border: none;
    transition: 0.2s;

    :hover {
        filter: brightness(1.1);
    }
`