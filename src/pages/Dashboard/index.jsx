import React, { useEffect, useState } from "react"
import { Container } from "./style"
import { useForm } from "react-hook-form"
import { yupResolver } from "@hookform/resolvers/yup"
import * as yup from "yup"
import { Redirect, useHistory } from "react-router-dom"
import Button from "../../components/Button"
import Card from "../../components/Card"
import api from "../../services/api"
import { toast } from "react-toastify"


const Dashboard = ({ id, authenticated, setAuthenticated, name }) => {

    const [att, setAtt] = useState(1)
    const [techs, setTechs] = useState([])
    const [token] = useState(JSON.parse(localStorage.getItem("@KenzieHub:token")) || "")

    const schema = yup.object().shape({
        title: yup.string().required("Digite o nome da tecnologia!"),
        status: yup.string().required("Campo Obrigatório!")
    })

    const { register, handleSubmit } = useForm({
        resolver: yupResolver(schema)
    })

    useEffect(() => {

        api
            .get(`/users/${id}`)
            .then(response => {
                const apiTechs = response.data.techs.map(item => ({
                    ...item,
                    created_at: new Date(item.created_at).toLocaleDateString("pt-BR", {
                        day: "2-digit",
                        month: "long",
                        year: "numeric",
                    }),
                    updated_at: new Date(item.created_at).toLocaleDateString("pt-BR", {
                        day: "2-digit",
                        month: "long",
                        year: "numeric",
                    }),
                }))
                setTechs(apiTechs)
            })
            .catch(err => console.log(err))

    }, [att])

    const submitFunction = (data) => {
        api
            .post("/users/techs", data, {
                headers: {
                    Authorization: `Bearer ${token}`,
                }
            })
            .then(response => {
                setAtt(att + 1)
                return toast.success(`Tecnologia adicionada`)
            })
            .catch(err => toast.error("Erro ao adicionar tecnologia, tente novamente mais tarde."))
    }

    const deleteTech = (id) => {
        const newTechs = techs.filter(item => item.id !== id)

        api
            .delete(`users/techs/${id}`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                }
            })
            .then(response => {
                setTechs(newTechs)
                return toast.error("Tecnologia excluida!")
            })
    }

    const history = useHistory()

    const logoff = () => {
        localStorage.clear()
        setTechs([])
        setAuthenticated(false)
        return history.push("/")
    }
    if (id === undefined) {
        logoff()
    }
    if (!authenticated) {
        return <Redirect to="/" />
    }

    return (
        <Container>
            <header>
                <h1>{name}</h1>
                <Button onClick={logoff}>Sair</Button>
            </header>
            <div className="techs">
                <form className="add_tech" onSubmit={handleSubmit(submitFunction)}>
                    <h2>Tecnologias:</h2>
                    <input placeholder="tech" name={"title"} {...register("title")} />
                    <input placeholder="status" name={"status"} {...register("status")} />
                    <button type="submit">Adicionar Tech</button>
                </form>
                <div className="cards">
                    {techs.map(item => <Card
                        token={token}
                        id={item.id}
                        title={item.title}
                        status={item.status}
                        created={item.created_at}
                        updated={item.updated_at}
                        deleteTech={deleteTech}
                    />)}
                </div>
            </div>
        </Container>
    )
}

export default Dashboard