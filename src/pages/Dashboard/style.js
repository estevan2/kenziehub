import styled from "styled-components";

export const Container = styled.main`
    header {
        display: flex;
        justify-content: space-between;
        padding: 10px;
    }

    .techs {
        display: flex;
        flex-direction: column;
        justify-content: flex-start;
        background-color: var(--blue-light);
        border: 1px solid var(--blue-bold);
        border-radius: 10px;
        margin: 40px 20px 0;
        padding: 10px;

        .add_tech {
            display: flex;
            justify-content: center;

            input {
                background-color: #fff;
                color: #000;
                font-size: 18px;
                border: 1px solid var(--blue);
                border-radius: 10px;
                height: 32px;
                width: 30%;
                padding-left: 25px;
                margin: 0px 10px;

                &::placeholder {
                    color: grey;
                }

                :focus {
                    outline: none;
                    border: 1px solid var(--blue-bold);
                }
            }
            button {
                background-color: var(--blue-bold);
                color: #fff;
                width: 164px;
                height: 32px;
                border-radius: 10px;
                border: none;
                transition: 0.2s;

                :hover {
                    filter: brightness(1.1);
                }
            }
        }
    }
    .cards {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        margin: 50px 0;
    }
`