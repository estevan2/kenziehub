import { Container, Msg } from "./style"
import Button from "../../components/Button"
import { Redirect, useHistory } from "react-router-dom"
import { useForm } from "react-hook-form"
import * as yup from "yup"
import { yupResolver } from "@hookform/resolvers/yup"
import api from "../../services/api"
import { toast } from "react-toastify"

const Login = ({ authenticated, setAuthenticated, setId, setName }) => {

    const schema = yup.object().shape({
        email: yup.string().required("Email Inválido"),
        password: yup.string().min(6, "Mínimo de 6 dígitos").required("Campo Obrigatório!")
    })

    const { register, handleSubmit, formState: { errors } } = useForm({
        resolver: yupResolver(schema)
    })

    const history = useHistory()

    const onSubmitFunction = (data => {
        api.post("/sessions", data).then(response => {
            const { token } = response.data
            const { id, name } = response.data.user

            setId(id)
            setName(name)

            localStorage.setItem("@KenzieHub:token", JSON.stringify(token))

            setAuthenticated(true)

            toast.success("Sucesso ao conectar!")

            return history.push("/dashboard")
        }).catch(err => toast.error("Email ou senha inválidos"))
    })

    if (authenticated) {
        return <Redirect to="/dashboard" />
    }

    return (
        <Container>
            <form onSubmit={handleSubmit(onSubmitFunction)}>
                <input
                    name={"email"}
                    placeholder="Email"
                    {...register("email")}
                />
                <Msg>{errors.email?.message}</Msg>
                <input
                    name={"password"}
                    placeholder="Senha"
                    {...register("password")}
                />
                <Msg>{errors.password?.message}</Msg>
                <Button type="submit">Entrar</Button>
                <p>Não possui uma conta? Clique em
                    <span onClick={() => history.push("/signup")}> Cadastrar!</span>
                </p>
            </form>
        </Container>
    )
}

export default Login