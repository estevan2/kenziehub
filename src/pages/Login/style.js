import styled from "styled-components";

export const Container = styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: var(--blue-light);
    border: 1px solid var(--blue-bold);
    border-radius: 10px;
    form{
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        width: 395px;
        height: 450px;
        padding: 40px;

        input {
            background-color: #fff;
            color: #000;
            font-size: 18px;
            border: 1px solid var(--blue);
            border-radius: 10px;
            height: 56px;
            width: 100%;
            padding-left: 25px;

            &::placeholder {
                    color: grey;
            }

            :focus {
                outline: none;
                border: 1px solid var(--blue-bold);
            }
        }

        button {
            margin-bottom: 20px;
        }

        p {
            margin-bottom: 20px;
            text-align: center;
            span {
                color: var(--blue-bold);
                cursor: pointer;
            }
        }
    }
`
export const Msg = styled.p`
    height: 10px;
    color: var(--red);
`