import Button from "../../components/Button"
import Input from "../../components/Input"
import { Enter, Container } from "./style"
import logo from "../../assets/logoblue.svg"
import { useHistory } from "react-router-dom"

const Home = () => {

    const history = useHistory()

    return (
        <>
            <Enter>
                <Button onClick={() => history.push("/login")}>Entrar</Button>
            </Enter>
            <Container>
                <div>
                    <h1>A tecnologia move o mundo.</h1>
                    <p>Um ser humano deve transformar informação em inteligência ou conhecimento. Tendemos a esquecer que nenhum computador jamais fará uma nova pergunta.</p>
                    <div>
                        <Button onClick={() => history.push("/signup")}>Cadastrar</Button>
                    </div>
                </div>
                <div className="logo">
                    <img src={logo} alt="logo" />
                </div>
            </Container>
        </>
    )
}

export default Home