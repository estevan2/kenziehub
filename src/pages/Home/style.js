import styled from "styled-components";

export const Enter = styled.div`
    display: flex;
    justify-content: flex-end;
    padding: 10px;
`

export const Container = styled.div`
    display: flex;
    justify-content: center;
    margin: 100px 0;

    div {
        max-width: 600px;

        h1 {
            font-size: 64px;
            margin-bottom: 30px;
        }

        p {
            margin-bottom: 30px;
            color: rgba(0, 0, 0, 0.75);
        }

        div {
            display: flex;
            
            button {
                height: 56px;
            }
        }
    }

    .logo {
        margin-left: 10%;

        img {
            width: 360px;        
        }
    }
`