import { Container, Msg } from "./style"
import Button from "../../components/Button"
import { Redirect, useHistory } from "react-router-dom"
import { useForm } from "react-hook-form"
import * as yup from "yup"
import { yupResolver } from "@hookform/resolvers/yup"
import api from "../../services/api"
import { toast } from "react-toastify"

const Signup = ({ authenticated }) => {

    const schema = yup.object().shape({
        name: yup.string().required("Campo Obrigatório!"),
        email: yup.string().required("Email Inválido!"),
        password: yup
            .string()
            .min(8, "Mínimo de 8 dígitos")
            .required("Campo Obrigatório!"),
        passwordConfirm: yup
            .string()
            .oneOf([yup.ref("password")], "Senhas diferentes")
            .required("Campo obrigatório"),
        bio: yup.string().required("Campo Obrigatório"),
        contact: yup.string().required("Campo Obrigatório"),
        course_module: yup.string().required("Campo Obrigatório"),
    })

    const { register, handleSubmit, formState: { errors } } = useForm({
        resolver: yupResolver(schema)
    })

    const history = useHistory()

    const onSubmitFunction = (data) => {

        console.log("click")

        // const user = { name, email, password, bio, contact, course_module }
        api.post("/users", data).then(_ => {
            toast.success("Sucesso ao criar conta!")
            return history.push("/login")
        }).catch(err => console.log(err))
    }

    if (authenticated) {
        return <Redirect to="/dashboard" />
    }

    return (
        <Container>
            <form onSubmit={handleSubmit(onSubmitFunction)}>
                <input
                    name={"name"}
                    placeholder="Nome"
                    {...register("name")}
                />
                <Msg>{errors.name?.message}</Msg>
                <input
                    name={"email"}
                    placeholder="Email"
                    {...register("email")}
                />
                <Msg>{errors.email?.message}</Msg>
                <input
                    name={"password"}
                    placeholder="Senha"
                    {...register("password")}
                />
                <Msg>{errors.password?.message}</Msg>
                <input
                    name={"passwordConfirm"}
                    placeholder="Confirmar Senha"
                    {...register("passwordConfirm")}
                />
                <Msg>{errors.passwordConfirm?.message}</Msg>
                <input
                    name={"bio"}
                    placeholder="bio"
                    {...register("bio")}
                />
                <Msg>{errors.bio?.message}</Msg>
                <input
                    name={"contact"}
                    placeholder="Contato"
                    {...register("contact")}
                />
                <Msg>{errors.contact?.message}</Msg>
                <input
                    name={"course_module"}
                    placeholder="módulo de curso"
                    {...register("course_module")}
                />
                <Msg>{errors.course_module?.message}</Msg>
                <Button type="submit">Cadastrar</Button>
                <p>Já possui uma conta? Clique em
                    <span onClick={() => history.push("/login")}> Entrar!</span>
                </p>
            </form>
        </Container>
    )
}

export default Signup