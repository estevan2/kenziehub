import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    :root {
        --blue-light: #F2F9F9;
        --blue-bold: #007AFF;
        --blue: #74ABE9;
        --red: #D90000;
    }

    body {
        background-color: var(--blue);
        color: #000;
        font-family: 'Roboto', sans-serif;
    }

    h1 {
        font-weight: 900;
    }

    h2 {
        font-weight: 400;
    }

    button {
        cursor: pointer;
    }

    a {
        text-decoration: none;
    }
`