import { Route, Switch } from "react-router-dom"
import Home from "../pages/Home"
import Signup from "../pages/Signup"
import Login from "../pages/Login"
import Dashboard from "../pages/Dashboard"
import { useEffect, useState } from "react"

const Routes = () => {

    const [authenticated, setAuthenticated] = useState(false)
    const [id, setId] = useState()
    const [userName, setUserName] = useState()

    useEffect(() => {
        const token = JSON.parse(localStorage.getItem("@KenzieHub:token"))

        if (token) {
            return setAuthenticated(true)
        }
    }, [authenticated])

    return (
        <Switch>
            <Route exact path="/">
                <Home />
            </Route>
            <Route path="/signup">
                <Signup authenticated={authenticated} />
            </Route>
            <Route path="/login">
                <Login authenticated={authenticated} setAuthenticated={setAuthenticated} setId={setId} setName={setUserName} />
            </Route>
            <Route path="/dashboard" >
                <Dashboard id={id} authenticated={authenticated} setAuthenticated={setAuthenticated} name={userName} />
            </Route>
        </Switch>
    )
}

export default Routes